import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import WelcomeStyles from '../styles/WelcomeStyles';

const WelcomeScreen = ({ navigation }) => {
  const handleGetStarted = () => {
    navigation.navigate('Login');
  };

  return (
    <View style={WelcomeStyles.container}>
      <Text style={WelcomeStyles.title}>Welcome to AmbatuProf</Text>
      <TouchableOpacity
        style={WelcomeStyles.getStartedButton}
        onPress={handleGetStarted}
      >
        <Text style={WelcomeStyles.buttonText}>GET STARTED</Text>
      </TouchableOpacity>
    </View>
  );
};

export default WelcomeScreen;