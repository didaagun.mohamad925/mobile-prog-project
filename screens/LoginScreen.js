import React, { useContext, useState } from 'react';
import { View, Text, TextInput, Button } from 'react-native';
import { SessionContext, SET_USER } from '../components/SessionContext';
import LoginStyles from '../styles/LoginStyles';

const LoginScreen = ({ navigation }) => {
  const { dispatch } = useContext(SessionContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginError, setLoginError] = useState(null);

  const handleLogin = () => {
    if (username === 'albert' && password === '123') {
      const user = { username };
      dispatch({ type: SET_USER, payload: user });
      navigation.navigate('UserProfile');
    } else {
      setLoginError('Login failed. Please check your credentials and try again.');
    }
  };

  return (
    <View style={LoginStyles.container}>
      <Text style={LoginStyles.title}>Login</Text>
      {loginError && <Text style={LoginStyles.errorMessage}>{loginError}</Text>}

      <TextInput
        style={LoginStyles.input}
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        style={LoginStyles.input}
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={setPassword}
      />
      <Button
        title="Login"
        onPress={handleLogin}
        style={LoginStyles.loginButton}
      />
    </View>
  );
};

export default LoginScreen;