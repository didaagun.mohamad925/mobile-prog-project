import React, { createContext, useContext, useReducer } from 'react';

const initialSessionState = {
  user: null,
  isLoggedIn: false,
};

const SessionContext = createContext(initialSessionState);

const SET_USER = 'SET_USER';
const LOGOUT = 'LOGOUT';

const sessionReducer = (state, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.payload,
        isLoggedIn: true,
      };
    case LOGOUT:
      return {
        ...state,
        user: null,
        isLoggedIn: false,
      };
    default:
      return state;
  }
};

const SessionProvider = ({ children }) => {
  const [sessionState, dispatch] = useReducer(sessionReducer, initialSessionState);

  return (
    <SessionContext.Provider value={{ sessionState, dispatch }}>
      {children}
    </SessionContext.Provider>
  );
};

export { SessionContext, SessionProvider, SET_USER, LOGOUT };